pub trait Inloptee: Sized {
    type Inner;

    const NONE: Self::Inner;

    fn from_inner(val: Self::Inner) -> Option<Self>;

    fn into_inner(self) -> Self::Inner;
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[repr(transparent)]
pub struct Inlopt<T: Inloptee>(T::Inner);

impl<T> Inlopt<T>
where
    T: Inloptee,
{
    pub const NONE: Self = Inlopt(T::NONE);

    #[inline]
    pub fn new(val: T::Inner) -> Self {
        // NOTE: Gets optimized out when NONE is the only invalid value.
        match T::from_inner(val) {
            Some(val) => Self(val.into_inner()),
            None => Self::NONE,
        }
    }

    #[inline]
    pub fn into_option(self) -> Option<T> {
        T::from_inner(self.0)
    }

    #[inline]
    pub fn from_option(val: Option<T>) -> Self {
        match val {
            Some(val) => Inlopt(val.into_inner()),
            None => Inlopt::NONE,
        }
    }
}

macro_rules! impl_elegible {
    ($(($Name:ident, $Inner:ident, $none:expr),)*) => {
        impl_elegible!($(($Name, $Inner, $none, |val| val != $none),)*);
    };
    ($(($Name:ident, $Inner:ident, $none: expr, $is_some:expr),)*) => {
        $(
            #[repr(transparent)]
            pub struct $Name($Inner);

            impl $Name {
                #[inline]
                pub fn new(val: $Inner) -> Option<Self> {
                    if $is_some(val) {
                        Some($Name(val))
                    } else {
                        None
                    }
                }

                #[inline]
                pub const unsafe fn new_unchecked(val: $Inner) -> Self {
                    $Name(val)
                }

                #[inline]
                pub fn get(self) -> $Inner {
                    self.0
                }
            }

            impl Inloptee for $Name {
                type Inner = $Inner;

                const NONE: Self::Inner = $none;

                #[inline]
                fn from_inner(val: Self::Inner) -> Option<Self> {
                    Self::new(val)
                }

                #[inline]
                fn into_inner(self) -> Self::Inner {
                    self.get()
                }
            }
        )*
    };
}

impl_elegible!(
    (NonMaxU8, u8, std::u8::MAX),
    (NonMaxU16, u16, std::u16::MAX),
    (NonMaxU32, u32, std::u32::MAX),
    (NonMaxU64, u64, std::u64::MAX),
    (NonMinI8, i8, std::i8::MIN),
    (NonMinI16, i16, std::i16::MIN),
    (NonMinI32, i32, std::i32::MIN),
    (NonMinI64, i64, std::i64::MIN),
);

impl_elegible!(
    (NonNegI8, i8, std::i8::MIN, |val| val >= 0),
    (NonNegI16, i16, std::i16::MIN, |val| val >= 0),
    (NonNegI32, i32, std::i32::MIN, |val| val >= 0),
    (NonNegI64, i64, std::i64::MIN, |val| val >= 0),
    (NonNanF32, f32, std::f32::NAN, |val: f32| !val.is_nan()),
    (NonNanF64, f64, std::f64::NAN, |val: f64| !val.is_nan()),
);
